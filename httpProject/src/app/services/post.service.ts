import { map } from 'rxjs/operator/map';
import { Post } from './../model/post';
import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
@Injectable()
export class PostService {

  url="https://api.github.com/users";
  constructor(private http: Http) {}

  public getAllPosts() {
    return this.http.get(this.url);
  }

  public addPost(post:Post) {
   return this.http.post(this.url, JSON.stringify(post));
  }

  public updatePost(post:Post) {
    return this.http.patch(this.url +'/'+post.id, {title:"hello 4twin"});
  }

  /**
   * delete
   */
  public delete(post:Post) {
    return this.http.delete(this.url +'/'+post.id);
  }
}
