import { PostService } from './../services/post.service';
import { Post } from './../model/post';
import { Component, OnInit } from '@angular/core';
import { Http, Response} from '@angular/http';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit{

  posts:any[];

  constructor(private ps:PostService) {}

   ngOnInit(){
    this.ps.getAllPosts().subscribe((resp:Response)=>{
      console.log(resp.json());
      this.posts=resp.json();
    })
   }

  //  createPost(input: HTMLInputElement){
  //  let post= new Post;
  //  post.title=input.value;
  //  input.value='';
  //     this.ps.addPost(post)
  //     .subscribe(response=>{
  //       console.log(response.json());
  //       post.id=response.json().id;
  //       //this.posts.push(post);
  //       this.posts.splice(0,0,post);

  //     })
  //  }



  //  updatePost(post){

  //   this.ps.updatePost(post)
  //   .subscribe(resp=>{
  //     console.log(resp.json());
  //     let index= this.posts.indexOf(post);
  //     this.posts.splice(index,1,resp.json());
  //   })


  //  }


  //  deletePost(post){
  //    this.ps.delete(post).subscribe(resp=>{
  //      this.posts.splice(this.posts.indexOf(post),1);
  //    })
  //  }
}
